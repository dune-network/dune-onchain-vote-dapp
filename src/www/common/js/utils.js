/* global parameters */

var backend_node = false;

var utils = {

    sleep: function(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    xhr_handler: function(url, endpoint, contentType, data, pp) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            var request = url + endpoint;
            var method = (data === null || data === undefined) ? 'GET' : 'POST';
            xhr.open(method, request, true);
            xhr.setRequestHeader('Content-Type', contentType);
            if (parameters.verbose) {
                console.log('making xhr ' + method + ' rpc ' + request + ' with data ' + data);
            }
            xhr.send(data);
            xhr.onload = function() {
                if (parameters.verbose) {
                    console.log('made xhr status = ' + xhr.status);
                    console.log('made xhr response = ' + xhr.response);
                }
                resolve({
                    success: (xhr.status >= 200 && xhr.status < 300),
                    status: xhr.status,
                    response: (pp === undefined || pp === null) ? xhr.response : pp(xhr.response)
                });
            };
            xhr.onerror = function() {
                if (parameters.verbose) {
                    console.log('failed xhr status = ' + xhr.status);
                    console.log('failed xhr response = ' + xhr.response);
                }
                reject({
                    success: false,
                    status: xhr.status,
                    response: 'Request to "' + request + '" failed. Is the resource down ?'
                });
            };
        });
    },

    xhr: function(url, endpoint, contentType, data, pp) {
        if (backend_node && url === parameters.node) {
            let new_url = parameters.apiEndpoint;
            let new_endpoint = '/nodeRPC?request=' + endpoint;
            return utils.xhr_handler(new_url, new_endpoint, contentType, data, pp);
        }
        return utils.xhr_handler(url, endpoint, contentType, data, pp);
    },

    getValue: function(id) {
        return document.getElementById(id).value;
    },

    setHTML: function(id, ctt) {
        if (document.getElementById(id) === null) {
            alert("utils.setHTML: id = " + id + " and content = " + ctt + "and ref is " + document.getElementById(id));
        }
        document.getElementById(id).innerHTML = ctt;
    },

    setValue: function(id, ctt) {
        document.getElementById(id).value = ctt;
    },

    showElt: function(id) {
        document.getElementById(id).style.display = 'block';
    },

    hideElt: function(id) {
        document.getElementById(id).style.display = 'none';
    },

    explorerLink: function(target, text, is_blank) {
        blank = is_blank ? ' target=_blank ' : ' ';
        return ('<a href="' + parameters.explorer + '/' + target + '"' + blank + '>' + text + '</a>');
    },

    urlParams: function() {
        var arr = {};
        window.location.search.substr(1).split("&").forEach(function(e) {
            s = e.split('=');
            arr[s[0]] = s[1];
        });
        return arr;
    },

    historyPage: function(hist_page) {
        try {
            if (hist_page === null || hist_page === undefined) {
                let params = utils.urlParams();
                let x = parseInt(params['history']);
                if (!Number.isInteger(x)) {
                    throw 'Not a Number';
                }
                return x;
            } else {
                let x = parseInt(hist_page);
                if (!Number.isInteger(x)) {
                    throw 'Not a Number';
                }
                return x;
            }
        } catch (_error) {
            return 1
        }
    },

    networkError: function(err) {
        console.log(err);
        msg = (err.response !== undefined) ? err.response : ((typeof err === "string") ? err : JSON.stringify(err));
        return (msg);
    },

    modelErrHandler: function(err) {
        utils.setWaitingMsg('<h2 style="overflow-wrap: break-word;">' + err + '</h2>', false).then(function() {});
    },

    loadingPage: '<img src="/vote-dapp/images/loading_Pedro_luis_romani_ruiz.gif"/> <h2>Loading page...</h2>',

    setWaitingMsg: function(content, autoReload) {
        return new Promise(function(resolve, _) {
            if (!autoReload &&
                document.getElementById('overlay-xxl-content').innerHTML !== content) {
                utils.setHTML('overlay-xxl-content', content);
                document.getElementById('overlay-xxl').style.display = 'block';
            }
            resolve();
        });
    },

    unsetWaitingMsg: function(autoReload) {
        return new Promise(function(resolve, _) {
            if (!autoReload) {
                utils.setHTML('overlay-xxl-content', '');
                document.getElementById('overlay-xxl').style.display = 'none';
            }
            resolve();
        });
    },

    contactUsBug: 'If you think this is a bug. Please, contract us at contact@dune.network.',
    metalNotDetected: 'Metal Browser Extension not installed or not detected!',
    metalNotConfigured: 'It seems Metal Browser Extension is not configured! Please, create or import a wallet.',
    metalGetAccountFailed: 'Failed to get Metal Browser Extension account!',


};
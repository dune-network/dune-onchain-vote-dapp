var metalModel = {

    state: {
        keyHash: null,
    },

    // metal.getAccount as a promise, because there is a bug in the async version currently
    __getMetalAccount: function() {
        return new Promise(
            function(resolve, reject) {
                try {
                    metal.getAccount(function(keyHash) {
                        resolve(keyHash)
                    }, function(error) { reject(error) });
                } catch (error) {
                    reject(err)
                }
            });
    },

    reset_state: function() {
        metalModel.state.keyHash = null;
    },

    connect: async function(force_get_account) {
        try {
            if (typeof metal === 'undefined' || metal === undefined) {
                metalModel.reset_state();
                if (force_get_account) {
                    throw (utils.metalNotDetected);
                }
                return null;
            }
            let is_enabled = await metal.isEnabled();
            if (!is_enabled) {
                metalModel.reset_state();
                if (force_get_account) {
                    throw (utils.metalNotConfigured);
                }
                return null;
            }
            let is_unlocked = await metal.isUnlocked();
            if (!is_unlocked && !force_get_account) {
                metalModel.reset_state();
                return null;
            }
            let keyHash = await metalModel.__getMetalAccount(); // metal.getAccount(); // async version buggy
            if (keyHash.charAt(0) === 'K') {
                msg = '';
                msg += 'Selected Metal address is ' + keyHash + '<br/>';
                msg += 'Only dn1 accounts are supported!';
                throw msg;
            };
            metalModel.state.keyHash = keyHash;
            return (JSON.parse(JSON.stringify(metalModel.state)));

        } catch (error) {
            console.log('pgMetal: ' + JSON.stringify(error));
            metalModel.reset_state();
            if (force_get_account) {
                throw error;
            }
            return null;
        }
    },

    getStateAsync: async function(force) {
        let s = metalModel.state;
        if (s.keyHash === null) {
            await metalModel.connect(force);
        }
        if (s.keyHash === null) {
            return null;
        }
        return (JSON.parse(JSON.stringify(metalModel.state))); // make a deep copy
    },

    getState: function() {
        let s = metalModel.state;
        if (s.keyHash === null) {
            return null;
        }
        return (JSON.parse(JSON.stringify(metalModel.state))); // make a deep copy
    },

    isMine: function(dn1) {
        return (metalModel.state !== null && metalModel.state.keyHash === dn1);
    }
};

var metalView = {

    refresh: function(data) {
        utils.hideElt('connecting-account');
        if (data === null) {
            utils.hideElt('accounts-info');
            utils.showElt('connect-metal');
        } else {
            let dn1 = data.keyHash === null ? '?' : utils.explorerLink(data.keyHash, data.keyHash, true);
            utils.setHTML('user-dn1-address', dn1);
            utils.hideElt('connect-metal');
            utils.showElt('accounts-info');
        }
    }
};

var metalController = {

    connectAndRefresh: async function(force_get_account) {
        try {
            let data = await metalModel.connect(force_get_account);
            metalView.refresh(data);
        } catch (error) {
            utils.modelErrHandler(error);
        }
    },

    connect: async function(force_get_account) {
        await metalController.connectAndRefresh(force_get_account);
    },

    intervalID: null,

    onStateChange: null,

    last_head_level: 0,
    nb_skipped_auto_reloads: 0,
    max_skipped_auto_reloads: 4,
    last_started_refresh: 0,
    last_finished_refresh: 0,
    last_started_refresh_time: 0,

    should_reload: async function(forceReload) {
        if (forceReload) {
            return forceReload;
        }
        let curr_block_lvl = parseInt((await nodeRPC.head(parameters.node)).response.header.level);
        if (curr_block_lvl !== metalController.last_head_level ||
            metalController.nb_skipped_auto_reloads > metalController.max_skipped_auto_reloads) {
            metalController.last_head_level = curr_block_lvl;
            metalController.nb_skipped_auto_reloads = 0;
            return true;
        }
        metalController.nb_skipped_auto_reloads++;
        return false;
    },

    init_pgMetal: async function(extra_func) {
        let aux = async function(autoReload, forceReload) {
            let do_reload = await metalController.should_reload(forceReload);
            if (do_reload) {
                await metalController.connect(false);
                if (extra_func !== undefined && extra_func !== null) {
                    await extra_func(autoReload);
                }
            }
        };
        await aux(false, true);
        metalController.intervalID =
            setInterval(
                // only refresh page once older refresh finished, or after 2 minutes (to deal with case where refresh failed/stopped because of an error)
                (async function() {
                    let now = new Date().getTime() / 1000;
                    if (metalController.last_started_refresh === metalController.last_finished_refresh ||
                        now - metalController.last_started_refresh_time > 120 // more than 2 minutes since last refresh
                    ) {
                        metalController.last_started_refresh++;
                        metalController.last_started_refresh_time = now;
                        await aux(true, false);
                        metalController.last_finished_refresh++;
                    }
                }), 20000
            );
        if (typeof metal !== 'undefined' && metal !== undefined) {
            metal.onStateChanged(async function() {
                await aux(true, true)
            });
        };
    }
};
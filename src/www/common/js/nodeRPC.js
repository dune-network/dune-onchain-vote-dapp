/* global parameters */
/* global utils */

var nodeRPC = {


    head: function(node_addr) {
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head",
            "application/json",
            null,
            JSON.parse
        );
    },

    manager_key: function(addr, node_addr) {
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + addr + "/manager_key",
            "application/json",
            null,
            JSON.parse
        );
    },

    balance: function(addr, node_addr) {
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + addr + "/balance",
            "application/json",
            null,
            JSON.parse
        );
    },

    storage: function(kt1, node_addr) {
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + kt1 + "/storage",
            "application/json",
            null,
            JSON.parse
        );
    },

    bigmapGet: function(data, bm_id, node_addr) {
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head/context/big_maps/" + bm_id,
            "application/json",
            data,
            JSON.parse
        );
    },

    mempool: function(node_addr) {
        return utils.xhr(
            node_addr,
            "/chains/main/mempool/pending_operations/",
            "application/json",
            null,
            JSON.parse
        );
    },

    callView: function (node_addr, kt1_addr, viewName, viewArg){
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head/context/contracts/" + kt1_addr + "/exec_fun/" + viewName,
            "application/json",
            viewArg,
            JSON.parse
        );

/*
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/check_not_already_played",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([game_id_encode, address_encode])))(game_id_0, address_1, 100000)));
        */
    },

    lastBlock: function(node_addr, position) {
        return utils.xhr(
            node_addr,
            "/chains/main/blocks/head-" + position,
            "application/json",
            null,
            JSON.parse
        );
    },
};

var nodeRpcHelpers = {

    trackOperation: function(node_addr, hash, from, to, max_depth) {
        let aux = function(e, position) {
            let tx = e.contents.find(op => op.kind === 'transaction'); // filter out reveals if any
            // do not track by hash anymore
            if ( /*e.hash === hash ||*/ (tx !== null && tx !== undefined && tx.source === from && tx.destination === to)) {
                epoint = tx.parameters === null ? "" : (tx.parameters.entrypoint === null ? "" : tx.parameters.entrypoint);
                param = tx.parameters === null ? null : (tx.parameters.value === null ? null : tx.parameters.value);
                let res = { tx: tx, position: position, hash: e.hash, entrypoint: epoint, parameter: param };
                throw res;
            }
            return false
        };
        let __trackOperationRec = function(node_addr, hash, from, to, i, max, resolve, reject) {
            if (i > max) {
                reject('Track Operation: max depth reached and op not found!');
            } else {
                nodeRPC.lastBlock(node_addr, i).then(function(res) {
                    try {
                        let _b = res.response.operations[3].some(e => aux(e, i)); // will return false or throw result
                        __trackOperationRec(node_addr, hash, from, to, i + 1, max, resolve, reject);
                    } catch (res) {
                        resolve(res); // 0 = head, 1 = head - 1, ..
                    }
                });
            }
        };
        return new Promise(function(resolve, reject) {
            nodeRPC.mempool(node_addr).then(function(res) {
                try {
                    let _b = res.response.applied.find(e => aux(e, -1)); // will return false or throw result
                    __trackOperationRec(node_addr, hash, from, to, 0, max_depth, resolve, reject);
                } catch (res) {
                    resolve(res); // -1 = mempool
                }
            });
        });
    },

    bigMapID: function(kt1_addr, toplevel_field_projection) {
        return new Promise(
            function(resolve, reject) {
                nodeRPC.storage(kt1_addr, parameters.node).then(function(res) {
                    try {
                        resolve(parseInt(toplevel_field_projection(res.response.dune_expr.record).bigmap.some));
                    } catch (err) {
                        reject(utils.networkError(err))
                    }
                }, function(err) { reject(utils.networkError(err)) });
            });
    }
}
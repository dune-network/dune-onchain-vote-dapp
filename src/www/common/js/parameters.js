// mainnet
var parameters = {
	verbose : false,
	network : 'mainnet',
	node : 'https://mainnet-node.dunscan.io:443',
	explorer : 'https://dunscan.io:443',
	apiEndpoint : 'http://127.0.0.1:3301',
	secondsBetweenBlocks : 60,
}

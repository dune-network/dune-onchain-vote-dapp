/* global utils */

var view = {



    ended_phase: function(p_id, details) {
        let pid = "phase-" + p_id;
        utils.setHTML(pid + "-info", details);
        var bar_id = pid + "-bar";
        var bar_elt = document.getElementById(bar_id);
        bar_elt.aria_valuenow = '100';
        bar_elt.style.width = '100%';
        utils.setHTML(bar_id, '100 %');
        bar_elt.classList.remove('progress-bar-animated');
        bar_elt.classList.remove('bg-info');
        bar_elt.classList.remove('bg-danger');
        bar_elt.classList.remove('bg-success');
        ended_class = 'bg-success';
        bar_elt.classList.add(ended_class);
        document.getElementById(pid + '-title').style.color = '#666';
    },

    current_phase: function(p_id, percentage, details) {
        let pid = "phase-" + p_id;
        utils.setHTML(pid + "-info", details);
        var bar_id = pid + "-bar";
        var bar_elt = document.getElementById(bar_id);
        bar_elt.aria_valuenow = percentage;
        bar_elt.style.width = percentage + '%';
        utils.setHTML(bar_id, percentage + '%');
        bar_elt.classList.remove('bg-danger');
        bar_elt.classList.remove('bg-success');
        bar_elt.classList.add('progress-bar-animated');
        bar_elt.classList.add('bg-info');
        document.getElementById(pid + '-title').style.color = '#000';

    },


    highlight_my_addr: function(addr) {
        return (metalModel.isMine(addr)) ? ('<span style="width:20px;margin: 0px; padding: 2px 5px" class="alert alert-success">' + addr + '</span>') : addr;
    },

    list_of_voters: function(res) {
        //utils.setHTML('number-of-participants', res.length);
        if (res.length === 0) {
            utils.setHTML('list-of-voters', '');
        } else {
            var msg = "";
            res.forEach(function(row) {
                var validator = view.highlight_my_addr(row.addr);
                validator = utils.explorerLink(row.addr, validator, true);
                msg += "<tr>";
                msg += "<td>" + validator + " </td>";
                msg += "<td>" + row.level + " </td>";
                msg += "<td>" + row.power + " </td>";
                msg += "<td>" + row.vote + " </td>";
                msg += "</tr>";
            });
            utils.setHTML('list-of-voters', msg);
        }
    },

    updatePage: async function(data) {
        let metalState = metalModel.getState();
        let contract_addr = utils.urlParams()['contract'];
        let curr_level = parseInt(data.head.header.level);
        let storage = data.storage.dune_expr.record;    
        let vote_start = parseInt (storage.vote_start.nat);
        var vote_end = parseInt (storage.vote_end.nat);
        //vote_end -= 856;

        let proposition = storage.proposition;
        let proposition_title = storage.title;
        let yes_votes = parseInt (storage.yes.nat);
        let no_votes = parseInt (storage.no.nat);
        let pass_votes = parseInt (storage.pass.nat);
        let total_eligible_rolls = parseInt (storage.total_eligible_rolls.nat);
        let voters = data.voters; // map or bigmap ??

        let cast_votes = yes_votes + no_votes + pass_votes;
        let cast_votes_perc = Number((cast_votes / total_eligible_rolls) * 100).toFixed(2);
        let remaining_to_end = vote_end - curr_level;
        utils.setHTML('proposition-url',`<a href="${proposition}">${proposition}</a>`);
        utils.setHTML('yes-votes',yes_votes);
        utils.setHTML('no-votes', no_votes);
        utils.setHTML('pass-votes', pass_votes);
        utils.setHTML('kt1-address', contract_addr);
        let kt1_url = utils.explorerLink(contract_addr, contract_addr, true);
        utils.setHTML('smart-contract-addr', kt1_url);
        utils.setHTML('view-title', proposition_title);
        
        if (cast_votes < total_eligible_rolls){
            view.current_phase(2, cast_votes_perc, cast_votes + " / " + total_eligible_rolls);
        }else{
            view.ended_phase(2, cast_votes + " / " + total_eligible_rolls);
        }
        let start_end_interval = `from block to ${vote_start} to block ${vote_end}`;
        document.getElementById('start-end-interval').title = start_end_interval;

        if (remaining_to_end >= 0){  // vote not ended yet
            utils.showElt('vote-open');
            utils.hideElt('vote-ended-message');
            utils.hideElt('vote-ended-winner');
            utils.hideElt('vote-ended-draw');
            let progress_perc = Number(((curr_level - vote_start) / (vote_end - vote_start)) * 100).toFixed(2);
            view.current_phase(1, progress_perc, remaining_to_end + " blocks left");

        }else{ // vote ended
            utils.hideElt('vote-open');
            utils.showElt('vote-ended-message');
            view.ended_phase(1, "ended " + (- remaining_to_end) + " blocks ago");
            tab = [{"vote": "YES", nb : yes_votes},{"vote": "NO", nb : no_votes}] // do not count PASS ,{"vote": "PASS", nb : pass_votes}
            tab.sort((a, b) => { return (b.nb - a.nb)});
            if (tab[0].nb > tab[1].nb){
                utils.hideElt('vote-ended-draw');
                utils.showElt('vote-ended-winner');
                utils.setHTML('vote-end-winner-choice', "<b>" + tab[0].vote + "</b>");
            }else{
                utils.hideElt('vote-ended-winner');
                utils.showElt('vote-ended-draw');
            }
        }

        view.list_of_voters(voters);


        ['no-rolls', 'already-voted', 'operation-in-mempool', 'operation-included', 'make-a-vote', 'command-line-vote'].forEach(element => {            
            utils.hideElt(element);
        });

        if (metalState === null) {
            utils.showElt('command-line-vote');
        }else{
            utils.setHTML('user-rolls', data.nb_rolls);
            if (data.nb_rolls === 0) {
                utils.showElt('no-rolls');
            } else {
                if (data.recent_operation !== undefined && data.recent_operation !== null) {
                    let op = data.recent_operation;
                    var op_hash = utils.explorerLink(op.op_hash, op.op_hash, true);
                    if (op.position === -1) { // mempool
                        utils.showElt('operation-in-mempool');
                        utils.setHTML('pending-op-hash', op_hash);
                    } else {
                        utils.showElt('operation-included');
                        utils.setHTML('op-hash', op_hash);
                        utils.setHTML('confirmation-blocks', op.position);
                    }
                } else {
                    if (data.has_voted) {
                        utils.showElt('already-voted');
                    } else {
                        utils.showElt('make-a-vote');
                    }
                }
            }
        }
    }
}
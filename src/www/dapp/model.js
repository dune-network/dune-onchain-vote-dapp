var model = {


    addressArg: function (addr) {
        return JSON.stringify({expr : { dune_expr: { address: addr}}, gas : "100000" });
    },

    natArg: function (n) {
        return JSON.stringify({expr : { dune_expr: { nat: '' + n + ''}}, gas : "100000" });
    },


    trackOp: async function (contract_addr, metalState) {
        try {
            // look in mempool, otherwise, OK ? also look in head ?
            //let trackDepth = Math.max(4, model.state.params.lock_player_x_blocks + 2);
            if (metalState === null || metalState.keyHash === null) {
                throw 'Metal not connected'
            }
            let trackDepth = 5; // XXX
            let last_op = await nodeRpcHelpers.trackOperation(parameters.node, null,
                metalState.keyHash, contract_addr, trackDepth);
            return {
                position: parseInt(last_op.position),
                op_hash: last_op.hash
            };
        } catch (error) {
            return null;
        }
    },

    updateBigmap: async function (contract_addr, from, to, arr){
        if (from > to){
            return arr
        }else{
            let arg = model.natArg(from);
            let e = await nodeRPC.callView(parameters.node, contract_addr, "get_vote", arg);
            let e2 = e.response.dune_expr.constr[1][0].tuple;
            arr.push (e2);
            let arr2 = await this.updateBigmap(contract_addr, from + 1, to, arr);
            return arr2
        }
    },

    refreshModel: async function () {
        let contract_addr = utils.urlParams()['contract'];
        let storage = await nodeRPC.storage(contract_addr, parameters.node);
        let head = await nodeRPC.head(parameters.node);
        votersArr = new Array();
        if (storage.response.dune_expr.record.voters.map !== null) {
            // let mp = storage.response.dune_expr.record.voters.map;
            let last_vote = parseInt(storage.response.dune_expr.record.last_vote.nat);
            let mp = await model.updateBigmap(contract_addr, 1, last_vote, []);
            mp.forEach(elt => {
                let addr = elt[0].addr;
                let level = parseInt(elt[1].record.level.nat);
                let power = parseInt(elt[1].record.power.nat);
                let vote = elt[1].record.vote.constr[0];
                let res = {
                    addr: addr,
                    level: level,
                    power: power,
                    vote: vote
                };
                votersArr.push(res);
            });
        }
        votersArr.sort((a, b) => {
            return (b.level - a.level);
        });

        let res = {
            storage: storage.response,
            head: head.response,
            voters: votersArr,
            nb_rolls: null,
            has_voted: null
        };
        let metalState = await metalModel.getStateAsync(false);
        if (metalState === null || metalState.keyHash === null) {
            return res;
        }

        let arg = model.addressArg(metalState.keyHash);
        let nb_rolls = await nodeRPC.callView(parameters.node, contract_addr, "nb_rolls", arg);
        let has_voted = await nodeRPC.callView(parameters.node, contract_addr, "has_already_voted", arg); // typo
        res.nb_rolls = parseInt(nb_rolls.response.dune_expr.nat);
        res.has_voted = has_voted.response.dune_expr;
        res.recent_operation = await model.trackOp(contract_addr, metalState);

        //res.nb_rolls = 1 ; // debug
        //res.has_voted = true ; // debug
        return res;
    },


    callEntrypoint: function(metalState, args) {
        return new Promise(
            function(resolve, reject) {
                if (typeof metal === 'undefined' || metal === undefined) {
                    reject(utils.metalNotDetected);
                } else {
                    metal.isEnabled(function(res) {
                        if (!res) {
                            reject(utils.metalNotConfigured);
                        }
                        let contract_addr = utils.urlParams()['contract'];
                        metal.getAccount(function(r) {
                            let op = {
                                dst: contract_addr,
                                amount: args.amount,
                                currency: args.currency,
                                network: parameters.network,
                                entrypoint: args.entrypoint,
                                parameter: args.param,
                                collect_call: false,
                                gas_limit: args.gas_limit,
                                storage_limit: args.storage_limit,
                                //type: 'batch',
                                cb: function(res) {
                                    console.log(res);
                                    resolve(res);
                                }
                            };
                            metal.send(op);
                        });
                    });
                }
            }
        );
    },

    /* debug mode
    vote: async function (choice){
        let metalState = await metalModel.getStateAsync(true);
        let par = '(dn1BAKER,'+choice+')';
        let param = '#love:' + par;
        let args = {
            amount: '0',
            currency: 'mudun',
            entrypoint: 'debug_cast_vote',
            param: param,
            gas_limit: '60000',
            storage_limit: '200'
        };
        res = await model.callEntrypoint(metalState, args);
        return res;
    },
    */

    vote: async function (choice){
        let metalState = await metalModel.getStateAsync(true);
        let param = '#love:' + choice;
        let args = {
            amount: '0',
            currency: 'mudun',
            entrypoint: 'cast_vote',
            param: param,
            gas_limit: '60000',
            storage_limit: '200'
        };
        res = await model.callEntrypoint(metalState, args);
        return res;
    }
}
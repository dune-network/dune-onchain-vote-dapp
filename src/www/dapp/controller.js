/* global model */
/* global view */

var controller = {

    showPage: function(autoReload, hist_page) {
        let hpage = utils.historyPage(hist_page);
        utils.setWaitingMsg(utils.loadingPage, autoReload).then(function() {
            hpage = Math.max(1, hpage);
            model.refreshModel().then(function(data) {
                    view.updatePage(data);
                    utils.unsetWaitingMsg(false /* not autoReload*/ ).then(function() {});
            }, utils.modelErrHandler).catch(utils.modelErrHandler);
        });
    },

    vote: function(choice) {
        model.vote(choice).then(function(res) {
            controller.showPage(true);
        }, function(err) {
            console.log(err);
            utils.modelErrHandler(err);
        });
    },


    play: function() {
        model.playChoice().then(function(res) {
            controller.showPage(true);
        }, function(err) {
            console.log(err);
            utils.modelErrHandler(err);
        });
    },

    revealSecret: function() {
        model.revealSecret().then(function(res) {
            controller.showPage(true);
        }, function(err) {
            console.log(err);
            utils.modelErrHandler(err);
        });
    },

    compensate: function() {
        model.compensate().then(function(res) {
            controller.showPage(true);
        }, function(err) {
            console.log(err);
            utils.modelErrHandler(err);
        });
    },


    denounce: function() {
        model.denounce().then(function(res) {
            controller.showPage(true);
        }, function(err) {
            console.log(err);
            utils.modelErrHandler(err);
        });
    },

    changeHistoryPage: function(delta) {
        let hpage = model.state.current_history_page + delta; // +1 or -1
        if (hpage <= 0)
            return;
        controller.showPage(false /* not autoReload*/ , hpage);
    },

    generateSecret: async function(force) {
        await utils.setWaitingMsg(utils.loadingPage, false);
        await model.generateSecret(force);
        controller.showPage(false); // update page
    },

    swapInsertSecret: async function() {
        await model.generateSecret(false); // genSecret without forcing if exists
        await model.swapInsertSecret();
        controller.showPage(true); // update page
    },

    swapPayFees: function() {
        model.swapPayFees();
    },

    swapNotifyPhasesChange: function() {
        model.swapNotifyPhasesChange();
    },

    changeRaffleKind: function(autoRefresh) {
        let kind = utils.getValue('raffle-kind');
        model.changeRaffleKind(kind);
        controller.showPage(autoRefresh); // update page -> not auto-reload -> waiting message
    },

    changeHistoryFilter: function(autoRefresh) {
        let history_filter = utils.getValue('history-filter');
        model.changeHistoryFilter(history_filter);
        controller.showPage(autoRefresh, 1); // update page -> not auto-reload -> waiting message
    },

    closePopUp: function() {
        view.closePopUp();
    },

    showPopUp: function(popup) {
        view.showPopUp(popup);
    },

    showGame: function(blockHash) {
        model.setDesiredGame(blockHash);
        controller.closePopUp();
        controller.showPage(false);
    }

};
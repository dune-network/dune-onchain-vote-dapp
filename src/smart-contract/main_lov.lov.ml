#love

type vote = Yes | No | Pass
type lvl = nat
type url = string

type vote_data = {
  level : lvl;
  vote : vote;
  power: nat
}

type storage = {
  vote_start : lvl;
  vote_end : lvl;
  title : string;
  proposition: url;
  (* The choice which receives the most votes among "Yes" "No" and
     "Pass" at the end of the vote wins *)
  yes : nat;
  no : nat;
  pass : nat;
  total_eligible_rolls : nat;
  voters : (address, vote_data) bigmap; (* TODO: move to bigmaps if it's
                                          too expensive *)
  last_vote : nat;
  voters_index : (nat, address) bigmap; (* from 1 to last_vote to ease crawling *)

  debug : bool;
}


val%init storage ((duration, title, proposition, debug) : (nat * string * url * bool)) =
  let vote_end = Current.level () ++ duration in
  let blocks_in_period = Current.blocks_per_voting_period () in
  let cur_vote_pos = Voting.position () in
  if cur_vote_pos ++ duration >=[:nat] blocks_in_period then
    failwith [:string * nat * nat * nat]
      ("The vote should start and finish in the same voting \
        period. Please wait or decrease the vote \
        duration. Voting.position() + duration => \
        blocks_per_voting_period()",
       cur_vote_pos, duration, Current.blocks_per_voting_period()) [:unit];
  {
    vote_start = Current.level ();
    vote_end = vote_end;
    proposition = proposition;
    title = title;
    yes = 0p;
    no = 0p;
    pass = 0p;
    total_eligible_rolls = Voting.total();
    voters = BigMap.empty [:address] [:vote_data];
    last_vote = 0p;
    voters_index = BigMap.empty [:nat] [:address];
    debug = debug;
  }

val%view nb_rolls storage (addr:address) : nat =
  match Keyhash.of_address addr with
  | None -> 0p (* KT1 address *)
  | Some pkh -> Voting.power pkh

val%view has_already_voted storage (addr:address) : bool =
  BigMap.mem [:address] [:vote_data] addr storage.voters


val%view get_vote storage (index:nat) : (address * vote_data) option =
  match BigMap.find [:nat] [:address] index storage.voters_index with
  | None -> None [:address * vote_data]
  | Some addr ->
    match BigMap.find [:address] [:vote_data] addr storage.voters with
    | None ->
      failwith [:string] "invariant: should not happen" [:(address * vote_data) option]
    | Some data ->
      Some (addr, data) [:address * vote_data]


val do_cast_vote (storage : storage) (vote : vote) (addr : address) : operation list * storage =
  let lvl = Current.level() in
  if (lvl >[:nat] storage.vote_end) then
    failwith [:string * nat] ("Closed at block: ", storage.vote_end) [:unit];
  if has_already_voted storage addr then
    failwith [:string * address] ("Address already voted: ", addr) [:unit];
  match Keyhash.of_address addr with
  | None ->
    failwith [:string * address]
      ("Address doesn't have rolls: ", addr) [:operation list * storage]
  | Some pkh ->
    if not (Voting.listed pkh) then
      failwith [:string * address] ("Address doesn't have rolls: ", addr) [:unit];
    let power = Voting.power pkh in
    let storage = match vote with
      | Yes -> { storage with yes = storage.yes ++ power }
      | No -> { storage with no = storage.no ++ power }
      | Pass -> { storage with pass = storage.pass ++ power }
    in
    let data = { level = lvl; vote = vote; power = power } in
    let last_vote = storage.last_vote ++ 1p in
    ([] [:operation]),
    { storage with
      last_vote = last_vote;
      voters = BigMap.add [:address] [:vote_data] addr data storage.voters;
      voters_index = BigMap.add [:nat] [:address] last_vote addr storage.voters_index}

val%entry cast_vote storage d (vote : vote) =
  do_cast_vote storage vote (Current.sender())


val%entry debug_cast_vote storage d ((addr, vote) : (address * vote)) =
  if storage.debug then
    do_cast_vote storage vote addr
  else
    failwith [:string] "This entrypoint can only be called in debug/dev mode"
      [:operation list * storage]

echo "++++"
echo usage: ./deploy.sh /path/to/ix/command deploy-account
echo "++++"
sleep 1
if [ "$IX" = "" ]; then
    if [ "$1" = "" ]; then
        echo "Please, export ix path in $IX shell variable or provide it as an argument to this script";
        exit 1
    fi
    IX=$1
fi

if [ "$2" = "" ]; then
    wallet=dun_ton
else
    wallet=$2
fi

IX=$1

IX="$IX client "

echo "CMD IS: "
echo "$IX originate contract vote-dapp transferring 0.000000 from $wallet running main_lov.lov.ml --init '#love:(900p, \"Test Vote\", \"https://dune.network/Test.pdf\", false)' --burn-cap 20 --force"

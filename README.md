# Dune Network Onchain Vote
## How to deploy the smart contract

Go to `src/smart-contract` and run the following command:
```
    ./deploy.sh /path/to/ix/command deploy-wallet
```

The deploy command will be displayed. Change the initialization parameters (title, url, duration, debug (true) or production(false) mode). Then run the command.


## How to build the website

Default parameters are set to mainnet (in `www/common/js/parameters.js`).

First, edit `www/dapp/main.html` and past the KT1 address obtained in the previous section in the provided template.

Then, run `make` to generate the website in `website` directory.

Start a local server inside `website` directory (with eg. `php -S localhost:8000`). Then, visit `localhost:8000/vote-dapp` in your browser.

## Licensing

The files in this repository (except the static ones in `website/` directory) are copyright [Origin-Labs](https://www.origin-labs.com), and are distributed under the terms of the MIT license (see LICENSE.txt for more details).

Static files in `website/` directory are part of the [Editorial template by HTML5 UP](https://html5up.net/editorial), and are licensed under the terms of the CCA 3.0 license (see website/README.txt and website/LICENSE.txt for more details).

## Contact us

Drop us an email at contact@origin-labs.com.
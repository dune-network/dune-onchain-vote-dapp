
.PHONY: all

all:
	mkdir -p website/vote-dapp/dapp
	cp -rf src/www/common/* website/vote-dapp/
	cp -rf src/www/dapp website/vote-dapp/
